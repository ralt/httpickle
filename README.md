# httpickle

An interactive terminal-based HTTP proxy.

## Current features

Enter an interactive session with `(start)` in the REPL, and inspect
incoming requests and their responses. You can also modify the headers
of these requests/responses.

## TODO

- A nice list of requests as they come in, with the ability to inspect
  them in-flight _or_ at a later time, if you're not blocking on
  them.
- Store all the requests in a temporary sqlite database during an
  interactive session.
- An API to be able to load scripts and eventually run the proxy in
  real-world scenarios.
- A better REPL (portacle?) so that you can really write scripts in
  slime, copy paste into a script, and never worry again.
- ncurses interface? Not sure.

## License

MIT License.
