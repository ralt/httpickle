(in-package #:httpickle)

(defvar *blocking* nil
  "Whether the requests should be blocking for interactivity.")
(defvar *blocking-lock* (make-instance 'seqlock)
  "Seqlock around *blocking*.")

(defvar *question* nil
  "The variable used to put questions in.")
(defvar *question-lock* (bt:make-lock "question lock")
  "Lock around *question*.")
(defvar *question-event* (bt:make-condition-variable :name "question event")
  "The condition variable to wait for new values in *question*.")

(defvar *answer* nil
  "The variable used to put answers in.")
(defvar *answer-lock* (bt:make-lock "answer lock")
  "Lock around *answer*.")
(defvar *answer-event* (bt:make-condition-variable :name "answer event")
  "The condition variable to wait for new values in *answer*.")

(defvar *request-lock* (bt:make-lock "request lock")
  "Lock around interactive requests.")

(defvar *acceptor* nil
  "Holds the hunchentoot acceptor instance.")

(defun cat (&rest args)
  (apply #'concatenate 'string args))

(defclass httpickle-acceptor (hunchentoot:acceptor)
  ())

(defmacro with-answer (vars &body body)
  (let ((answer-var (first vars))
	(question (second vars))
	(acquire-lock (third vars)))
    `(progn
       (bt:with-lock-held (*question-lock*)
	 (setf *question*
	       ,question))
       (bt:condition-notify *question-event*)
       (if ,acquire-lock
	   (bt:with-lock-held (*answer-lock*)
	     (bt:condition-wait *answer-event* *answer-lock*)
	     (let ((,answer-var *answer*))
	       ,@body))
	   (progn
	     (bt:condition-wait *answer-event* *answer-lock*)
	     (let ((,answer-var *answer*))
	       ,@body))))))

(defmethod hunchentoot:acceptor-dispatch-request ((acceptor httpickle-acceptor) request)
  (unless (with-reader-lock *blocking-lock* *blocking*)
    (return-from hunchentoot:acceptor-dispatch-request
      (send-request request (hunchentoot:headers-in* request))))
  (bt:with-lock-held (*request-lock*)
    (let ((show-title t)
	  (request-headers (hunchentoot:headers-in* request)))
      (loop
	 (with-answer (answer
		       (make-question
			(if show-title
			    (format
			     nil
			     (cat
			      "Intercepting request going to ~a~%"
			      "What do you want to do with this intercepted request?")
			     (hunchentoot:request-uri request))
			    "What do you want to do with this request?")
			(list (make-question-item 'print-headers "Print the headers and their values")
			      (make-question-item 'change-header "Change a header's value")
			      (make-question-item 'send-request "Send the request upstream")))
		       t)
	   (setf show-title nil)
	   (cond ((eq answer 'print-headers)
		  (format t
			  "~{~a~%~}"
			  (mapcar (lambda (header)
				    (format nil "~a: ~a" (car header) (cdr header)))
				  request-headers)))

		 ((eq answer 'change-header)
		  (with-answer (header-name
				(make-question
				 "Which header do you want to change?"
				 (mapcar
				  (lambda (header)
				    (make-question-item
				     (car header)
				     (format nil "~a" (car header))))
				  request-headers))
				nil)
		    (with-answer (header-value
				  (make-question
				   (format nil "Which value do you want for ~a: " header-name))
				  nil)
		      (setf request-headers
			    (mapcar (lambda (header)
				      (cons (car header)
					    (if (eq header-name (car header))
						header-value
						(cdr header))))
				    request-headers)))))

		 ((eq answer 'send-request)
		  (return-from hunchentoot:acceptor-dispatch-request
		    (send-request request request-headers)))))))))

(defun send-request (request request-headers)
  (multiple-value-bind (upstream-body-stream
			return-code
			response-headers
			uri
			stream
			must-close)
      (drakma:http-request
       (hunchentoot:request-uri request)
       :method (hunchentoot:request-method* request)
       :additional-headers (hunchentoot:headers-in* request)
       :force-binary t
       :want-stream t
       :content (lambda (upstream-stream)
		  (let ((content-length-header-string
			 (hunchentoot:header-in :content-length request)))
		    (when (and content-length-header-string
			       (> (parse-integer content-length-header-string) 0))
		      (let ((downstream-request-body-stream
			     (hunchentoot:raw-post-data :want-stream t)))
			(loop for i from 0 below (parse-integer content-length-header-string)
			   do (write-byte (read-byte downstream-request-body-stream)
					  upstream-stream)))))))
    (declare (ignore uri stream))
    (handle-response request
		     upstream-body-stream
		     return-code
		     response-headers
		     must-close)))

(defun handle-response (request upstream-body-stream return-code response-headers must-close)
  (unless (with-reader-lock *blocking-lock* *blocking*)
    (return-from handle-response
      (send-response upstream-body-stream
		     return-code
		     response-headers
		     must-close)))

  (let ((show-title t))
    (loop
       (with-answer (answer
		     (make-question
		      (if show-title
			  (format
			   nil
			   (cat
			    "Intercepting response coming back from ~a~%"
			    "What do you want to do with this intercepted response?")
			   (hunchentoot:request-uri request))
			  "What do you want to do with this response?")
		      (list
		       (make-question-item 'print-headers "Print the headers and their values")
		       (make-question-item 'send-response "Send the response downstream")))
		     nil)
	 (setf show-title nil)
	 (cond ((eq answer 'print-headers)
		(format t
			"~{~a~%~}~%"
			(mapcar
			 (lambda (header)
			   (format nil
				   "~a: ~a"
				   (car header)
				   (cdr header)))
			 response-headers)))
	       ((eq answer 'send-response)
		(return-from handle-response
		  (send-response upstream-body-stream
				 return-code
				 response-headers
				 must-close))))))))

(defun send-response (upstream-body-stream return-code response-headers must-close)
  (setf (hunchentoot:return-code*) return-code)
  (dolist (header response-headers)
    (setf (hunchentoot:header-out (car header))
	  (if (eq (car header) :content-length)
	      (parse-integer (cdr header))
	      (cdr header))))
  (let ((downstream-body-stream (hunchentoot:send-headers)))
    (loop for byte = (read-byte (flexi-streams:flexi-stream-stream upstream-body-stream)
				nil
				'eof)
       until (eq byte 'eof)
       do (write-byte byte downstream-body-stream)))
  (when must-close
    (close upstream-body-stream)))

(defun main ()
  (setf hunchentoot:*catch-errors-p* nil)
  (setf *acceptor* (make-instance 'httpickle-acceptor
				  :port (parse-integer (uiop:getenv "PORT"))))
  (hunchentoot:start *acceptor*)

  (loop
     (with-handle-interrupt
	 (with-writer-lock *blocking-lock*
	   (setf *blocking* nil))
       (if (with-reader-lock *blocking-lock* *blocking*)
	   (block question-block
	     (loop
		(bt:with-lock-held (*question-lock*)
		  (bt:condition-wait *question-event* *question-lock*)
		  (let ((answer (ask *question*)))
		    (bt:with-lock-held (*answer-lock*)
		      (setf *answer* answer))
		    (bt:condition-notify *answer-event*))
		  (unless (with-reader-lock *blocking-lock* *blocking*)
		    (return-from question-block)))))
	   (progn
	     (format *query-io* "~%> ")
	     (force-output *query-io*)
	     (let* ((code (read-line))
		    (value (let ((*package* (find-package "HTTPICKLE-USER")))
			     (eval
			      (read-from-string code)))))
	       (unless (with-reader-lock *blocking-lock* *blocking*)
		 (format t "~a~%" value))))))))

(defun quit ()
  (format t "Shutting down the HTTP proxy...~%")
  (hunchentoot:stop *acceptor*)
  (uiop:quit))

(defun start ()
  (format t "Starting interactive session.~%")
  (with-writer-lock *blocking-lock*
    (setf *blocking* t)))
