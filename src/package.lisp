(defpackage #:httpickle
  (:use #:cl)
  (:export #:quit
	   #:start))

(defpackage #:httpickle-user
  (:use #:cl #:httpickle))
