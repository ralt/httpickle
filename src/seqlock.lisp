(in-package #:httpickle)

(defclass seqlock ()
  ((writer-lock :accessor writer-lock)
   (counter :accessor counter :initform 0)))

(defmethod initialize-instance :after ((seqlock seqlock) &key)
  ;; Needs to be recursive because we acquire it in a signal handler.
  (setf (writer-lock seqlock) (bt:make-recursive-lock "seqlock writer lock")))

(defmacro with-reader-lock (lock &body body)
  (let ((loop-block (gensym)))
    `(block ,loop-block
       (loop
	  (let ((original-value (counter ,lock)))
	    (unless (oddp original-value)
	      (let ((result (progn ,@body)))
		(when (= original-value (counter ,lock))
		  (return-from ,loop-block result)))))))))

(defmacro with-writer-lock (lock &body body)
  `(bt:with-lock-held ((writer-lock ,lock))
     (incf (counter ,lock))
     (unwind-protect
	  (progn ,@body)
       (incf (counter ,lock)))))
