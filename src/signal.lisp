(in-package #:httpickle)

(defmacro with-handle-interrupt (handler &body body)
  `(handler-case
       #-ccl (progn ,@body)
       #+ccl (let ((ccl:*break-hook* (lambda (c h)
				       (declare (ignore h))
				       (error c))))
	       ,@body)
       (#+ccl ccl:interrupt-signal-condition
	#+sbcl sb-sys:interactive-interrupt
	()
	,handler)))
