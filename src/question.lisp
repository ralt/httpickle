(in-package #:httpickle)

(defclass question-item ()
  ((symbol :initarg :symbol :reader question-symbol)
   (text :initarg :text :reader question-text)))

(defclass multiple-items-question ()
  ((title :initarg :title :reader title)
   (items :initarg :items :reader items)))

(defclass user-provided-answer-question ()
  ((title :initarg :title :reader title)))

(defgeneric ask (question)
  (:documentation "Ask a question to the user."))

(defmethod ask ((question multiple-items-question))
  (format *query-io* "~%~a~%~%" (title question))
  (loop for i from 0 below (length (items question))
     do (format *query-io* "[~a] ~a~%" i (question-text (elt (items question) i))))
  (loop
     (handler-case
	 (progn
	   (format *query-io* "~%Pick your poison: ")
	   (force-output *query-io*)
	   (let ((answer (question-symbol
			  (elt (items question) (parse-integer (read-line))))))
	     (format t "~%")
	     (return-from ask answer)))
       (error () nil))))

(defmethod ask ((question user-provided-answer-question))
  (format *query-io* "~%~a" (title question))
  (force-output *query-io*)
  (read-line))

(defun make-question (title &optional (items nil))
  (if (> (length items) 0)
      (make-instance 'multiple-items-question
		     :title title
		     :items items)
      (make-instance 'user-provided-answer-question
		     :title title)))

(defun make-question-item (symbol text)
  (make-instance 'question-item
		 :symbol symbol
		 :text text))
