SOURCES := $(wildcard src/*.lisp) $(wildcard *.asd) $(wildcard t/*.lisp)

all: httpickle

httpickle: $(SOURCES)
	@ros run -- \
		--eval "(ql:quickload :httpickle)" \
		--eval "(asdf:oos 'asdf:program-op :httpickle)" \
		--eval "(quit)"
