(defsystem #:httpickle
  :description "An interactive terminal-based HTTP proxy."
  :license "GPLv3"
  :depends-on (:uiop :hunchentoot :drakma :bordeaux-threads :trivial-signal)
  :entry-point "httpickle::main"
  :build-operation "program-op"
  :components ((:module
		"src"
		:components
		((:file "package")
		 (:file "main" :depends-on ("package"
					    "seqlock"
					    "question"
					    "signal"))
		 (:file "seqlock" :depends-on ("package"))
		 (:file "question" :depends-on ("package"))
		 (:file "signal" :depends-on ("package"))))))
